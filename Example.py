import ui

with ui.ImageContext(100, 100) as ctx:
	ui.set_color('white')
	ui.fill_rect(0, 0, 100, 100)
	ui.set_color('red')
	circle = ui.Path.oval(10, 10, 80, 80)
	circle.fill()
	img = ctx.get_image()
	img.show()
