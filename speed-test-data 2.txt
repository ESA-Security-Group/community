{
  "channel" : null,
  "signal_dbm" : null,
  "channel_util" : {
    "tx" : null,
    "total" : null,
    "rx" : null
  },
  "upload" : {
    "history" : [

    ],
    "max" : 0,
    "std_dev" : 0,
    "avg" : 0
  },
  "rx_rate" : null,
  "timestamp" : "2019-12-06T14:40:27+01:00",
  "download" : {
    "avg" : 0,
    "max" : 0,
    "std_dev" : 0,
    "history" : [

    ]
  },
  "tx_rate" : null,
  "fw_version" : null,
  "model" : null
}