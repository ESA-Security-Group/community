; <<>> DiG 9.9.5-9+deb8u16-Debian <<>> @8.8.8.8 8.8.8.8 ANY -c IN
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NXDOMAIN, id: 32119
;; flags: qr rd ra ad; QUERY: 1, ANSWER: 0, AUTHORITY: 1, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;8.8.8.8.			IN	ANY

;; AUTHORITY SECTION:
.			86398	IN	SOA	a.root-servers.net. nstld.verisign-grs.com. 2019083102 1800 900 604800 86400

;; Query time: 11 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
;; WHEN: Sat Aug 31 19:45:47 CEST 2019
;; MSG SIZE  rcvd: 111